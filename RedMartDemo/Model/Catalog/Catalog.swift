import Foundation

struct Catalog: Codable {
    let products: [Product]?
    let facets: Facets?
    let filters: Filters?
    let onSaleCount: Int?
    let status: Status?
    let total: Int?
    let page: Int?
    let pageSize: Int?
    let meta: Meta?
    let title: String?
    let images: SaleImages?
}

struct Status: Codable {
    let msg: String?
    let code: Int?
}

struct Meta: Codable {
    let title: String?
    let desc: String?
    let keywords: String?
}

struct SaleImages: Codable {
    let icon: Icon?
}

struct Icon: Codable {
    let url: String?
}
