import Foundation

struct ProductProfile: Codable {
    let product: Product?
    let total: Int?
    let pageSize: Int?
    let session: String?
    let page: Int?
    let status: Status?
}
