import Foundation

struct Facets: Codable {
    let categories: [Category]?
}

struct Category: Codable {
    let id: Int?
    let name: String?
    let uri: String?
    let count: Int?
    let image: String?
}
