import Foundation

struct Filters: Codable {
    let options: [Option]?
    let toggles: [Toggle]?
}

struct Toggle: Codable {
    let name: String?
    let uri: String?
}

struct Option: Codable {
    let name: String?
    let uri: String?
    let types: [OptionType]?
}

struct OptionType: Codable {
    let count: Int?
    let name: String?
    let uri: String?
}
