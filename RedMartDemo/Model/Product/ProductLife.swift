import Foundation

struct ProductLife: Codable {
    let time: Int?
    let metric: String?
    let isMinimum: Bool?
    let timeIncludingDelivery: Int?
}
