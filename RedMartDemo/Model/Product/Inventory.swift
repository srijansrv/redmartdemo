import Foundation

struct Inventory: Codable {
    let atpStatus: Int?
    let maxSaleQty: Int?
    let qtyInCarts: Int?
    let qtyInStock: Int?
    let stockStatus: Int?
    let stockType: Int?
    let nextAvailableDate: String?
    let limitedStockStatus: Int?
    let atpLots: [ATPLot]?
    let deliveryOption: String?
    //let __v: Int?
    let originalMaxSaleQty: Int?
}

struct ATPLot: Codable {
    let fromDate: String?
    let toDate: String?
    let stockStatus: Int?
    let qtyInStock: Int?
    let qtyInCarts: Int?
}
