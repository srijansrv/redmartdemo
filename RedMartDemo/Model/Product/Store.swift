import Foundation

struct Store: Codable {
    let id: Int?
    let name: String?
    let storeType: String?
    let desc: String?
    let mediaAssets: MediaAssets?
    let uri: String?
}

struct MediaAssets: Codable {
    let logo: Logo?
}

struct Logo: Codable {
    let name: String?
    let url: String?
}
