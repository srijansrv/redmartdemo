import Foundation

struct Details: Codable {
    let prodType: Int?
    let uri: String?
    let status: Int?
    let isNew: Int?
    let storageClass: String?
    let countryOfOrigin: String?
}
