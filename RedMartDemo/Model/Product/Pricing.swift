import Foundation

struct Pricing: Codable {
    let promoId: Int?
    let onSale: Int?
    let price: Double?
    let promoPrice: Double?
    let savingsType: Int?
    let savingsText: String?
    let savings: Double?
    let savingsAmount: Double?
    let discounts: Discounts?
    let applicableDiscount: String?
}

struct Discounts: Codable {
    let promo: LiveUp?
    let liveUp: LiveUp?
}

struct LiveUp: Codable {
    let promoPrice: Double?
    let savings: Double?
    let savingsAmount: Double?
    let savingsType: Int?
    let savingsText: String?
}
