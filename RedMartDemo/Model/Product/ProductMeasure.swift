//
//  Measure.swift
//  RedMartDemo
//
//  Created by Srijan Srivastava on 8/10/18.
//  Copyright © 2018 Srijan Srivastava. All rights reserved.
//

import Foundation

struct ProductMeasure: Codable {
    let weightOrVolume: String?
    let size: Int?
    
    enum CodingKeys: String, CodingKey {
        case weightOrVolume = "wt_or_vol"
        case size
    }
}


