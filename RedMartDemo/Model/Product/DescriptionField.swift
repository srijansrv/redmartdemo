import Foundation

struct DescriptionField: Codable {
    let secondary: [Description]?
    let primary: [Description]?
}

struct Description: Codable {
    let name: String?
    let content: String?
}
