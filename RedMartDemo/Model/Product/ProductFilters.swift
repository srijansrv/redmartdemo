import Foundation

struct ProductFilters: Codable {
    let isOrganic: Int?
    let brandUri: String?
    let mfrName: String?
    let frequency: Int?
    let brandName: String?
    let vendorName: String?
    let countryOfOrigin: String?
    let trendingFrequency: Int?
    let natural: Int?
    let japanese: Int?
    let sugarFree: Int?
    let goat: Int?
    let nutFree: Int?
    let greekYoghurt: Int?
    let lessSugar: Int?
    let wheatFree: Int?
    let korean: Int?
    let multigrain: Int?
    let madeInSingapore: Int?
    let preservativeFree: Int?
    let lowfat: Int?
    let nonDairy: Int?
    let halal: Int?
    let sanitaryPadSize: String?
    let vendor: Int?
    let antibacterial: Int?
    let festiveBbq: String?
    let tasteProfile: String?
    let grape: String?
    let wineRegion: String?
    let format: String?
    let organicbiodynamic: Int?
    let holidayParty: String?
    let festiveDinner: String?
    let vegan: Int?
}
