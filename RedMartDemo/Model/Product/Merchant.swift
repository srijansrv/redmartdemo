import Foundation

struct Merchant: Codable {
    let subVendorId: Int?
    let subVendorName: String?
    let subVendorLogo: Image?
}
