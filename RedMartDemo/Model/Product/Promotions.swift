import Foundation

struct Promotions: Codable {
    let id: Int?
    let type: Int?
    let savingsText: String?
    let promoLabel: String?
    let currentProductGroupId: Int?
    let products: [PromotedProducts]?
    let liveUpSavingsText: String?
}

struct PromotedProducts: Codable {
    let images: [Image]?
    let groupId: Int?
    let minQty: Int?
    let groupName: String?
}
