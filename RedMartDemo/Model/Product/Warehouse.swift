import Foundation

struct Warehouse: Codable {
    let measure: WarehouseMeasure?
}

struct WarehouseMeasure: Codable {
    let volMetricAlt: String?
    let volMetric: String?
    let wt: Double?
    let wtMetric: String?
    let l: Int?
    let w: Int?
    let h: Int?
    let vol: Double?
}
