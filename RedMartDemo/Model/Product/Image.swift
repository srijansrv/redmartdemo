import Foundation

struct Image: Codable {
    let h: Int?
    let w: Int?
    let name: String?
    let position: Int?
}
