import Foundation

struct Product: Codable {
    let categoryTags: [String]?
    let id: Int?
    let title: String?
    let desc: String?
    let sku: String?
    let categories: [Int]?
    let types: [Int]?
    let details: Details?
    let filters: ProductFilters?
    let img: Image?
    let images: [Image]?
    let measure: ProductMeasure?
    let pricing: Pricing?
    let warehouse: Warehouse?
    let attributes: Attributes?
    let descriptionFields: DescriptionField?
    let productLife: ProductLife?
    let promotions: [Promotions]?
    let pr: Int?
    let inventory: Inventory?
    let inventories : [Inventory]?
    let merchant: Merchant?
    let store: Store?
    let labels: [String]?
}

struct ProductMeasure: Codable {
    let wtOrVol: String?
    let size: Double?
}
