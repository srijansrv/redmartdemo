import Foundation
import UIKit

class ServiceManager: URLSession, URLSessionDelegate {
    let  timeout = 60.0
    let  contentType = "application/json"
    let  cachePolicy = NSURLRequest.CachePolicy.useProtocolCachePolicy
    
    private func getData(from url:URL, completionHandler: @escaping (Data?, Error?) -> Void) {
        var request = URLRequest(url: url, cachePolicy: cachePolicy, timeoutInterval: timeout)
        request.httpMethod = "GET"
        request.addValue(contentType, forHTTPHeaderField: "Content-Type")
        
        let sessionConfiguration = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfiguration)
        
        let dataTask = session.dataTask(with: request) { (data, response, error) in
            if let  serviceError = error {
                completionHandler(nil, serviceError as Error)
            } else {
                if let  responseData = data {
                    completionHandler(responseData, nil)
                }
            }
        }
        dataTask.resume()
    }
    
    private func downloadData(from url: URL, completionHandler: @escaping (UIImage?, Error?) -> Void) {
        let sessionConfiguration = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfiguration)
        
        let downloadTask = session.downloadTask(with: url) { (retrievedURL, response, error) in
            if let serviceError = error {
                completionHandler(nil, serviceError as Error)
            } else {
                if let responseURL = retrievedURL {
                    let data = try! Data(contentsOf: responseURL)
                    let image = UIImage(data: data)
                    completionHandler(image, nil)
                }
            }
        }
        downloadTask.resume()
    }
    
    func getAllProductsFor(page: Int, completionHandler: @escaping (Catalog?, Error?) -> Void) {
        let pageSize = 30
        let  urlString = "https://api.redmart.com/v1.6.0/catalog/search?theme=all-sales&pageSize=\(pageSize)&page=\(page)"
        let  catalogURL = URL(string: urlString)!
        
        getData(from: catalogURL) { (data, error) -> Void in
            guard let  catalogData = data else {
                return
            }
            do {
                let  decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let catalog = try decoder.decode(Catalog.self, from: catalogData)
                completionHandler(catalog, nil)
            } catch {
                completionHandler(nil, error)
            }
        }
    }
    
    func getImageForProduct(from url: String, completionHandler: @escaping (UIImage?, Error?) -> Void) {
        let url = URL(string: url)
        downloadData(from: url!) { (image, error) in
            guard let downloadedImage = image else {
                completionHandler(nil, error)
                return
            }
            DispatchQueue.main.async {
                completionHandler(downloadedImage, nil)
            }
        }
    }
    
    func getProductProfile(productId: Int, completionHandler: @escaping (ProductProfile?, Error?) -> Void) {
        let urlString = "http://api.redmart.com/v1.6.0/products/\(productId)"
        let productProfileURL = URL(string: urlString)!
        
        getData(from: productProfileURL) { (data, error) -> Void in
            guard let  productData = data else { return }
            do {
                let decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let productProfile = try decoder.decode(ProductProfile.self, from: productData)
                completionHandler(productProfile, nil)
            } catch {
                print(error.localizedDescription)
                completionHandler(nil, error)
            }
        }
    }
}
