import UIKit

class CatalogViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    @IBOutlet weak var collectionView: UICollectionView!
    var catalog: Catalog?
    var error: Error?
    var imageCache = [Int: UIImage]()
    let imageBaseUrl = "http://media.redmart.com/newmedia/200p"
    let CatalogCellIdentifier = "CatalogCell"
    let manager = ServiceManager()
    var selectedProduct: Product?
    let initialPage = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: CatalogCellIdentifier, bundle: nil), forCellWithReuseIdentifier: CatalogCellIdentifier)
        getProductsCatalogFor(page: initialPage)
    }
    
    func getProductsCatalogFor(page: Int) {
        manager.getAllProductsFor(page: page) { [unowned self] (catalog, error) in
            guard let newCatalog = catalog else {
                self.error = error
                //call method to show error alert
                return
            }
            
            if let existingProducts = self.catalog?.products {
                let updatedProducts: [Product] = existingProducts + (newCatalog.products)!
                self.catalog = Catalog(products: updatedProducts, facets: newCatalog.facets,
                                       filters: newCatalog.filters, onSaleCount: newCatalog.onSaleCount,
                                       status: newCatalog.status, total: newCatalog.total,
                                       page: newCatalog.page, pageSize: newCatalog.pageSize,
                                       meta: newCatalog.meta, title: newCatalog.title, images: newCatalog.images)
            } else {
                self.catalog = catalog
            }

            DispatchQueue.main.async { [unowned self] in
                self.collectionView.reloadData()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "CatalogToProduct") {
            let productVC: ProductViewController = segue.destination as! ProductViewController
            productVC.product = selectedProduct
        }
    }
    
    //MARK: Collection View Methods
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return catalog?.products?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let product = catalog?.products?[indexPath.row] else {
            return UICollectionViewCell()
        }
        
       let cell: CatalogCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: CatalogCellIdentifier, for: indexPath) as! CatalogCollectionViewCell
        cell.nameLabel.text = product.title
        cell.priceLabel.text = "\(product.pricing?.price ?? 0)"
        cell.savingsLabel.text = product.pricing?.savingsText
        updateImageFor(cell: cell, at: indexPath, with: product)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedProduct = catalog?.products![indexPath.row]
        performSegue(withIdentifier: "CatalogToProduct", sender: self)
    }
    
    //MARK: Fetch Image Asynchronously
    func updateImageFor(cell: CatalogCollectionViewCell, at indexPath: IndexPath, with product: Product) {
        var imageURL = imageBaseUrl
        let endPoint = product.img?.name ?? ""
        imageURL = imageBaseUrl + endPoint
        
        if(imageCache[product.id!] != nil) {
            cell.productImage.image = imageCache[product.id!]
        } else {
            manager.getImageForProduct(from: imageURL, completionHandler: { [unowned self] (image, error) in
                if( image != nil) {
                    self.imageCache[product.id!] = image
                    DispatchQueue.main.async {
                        cell.productImage.image = image
                        cell.productImage.contentMode = .scaleAspectFit
                        self.collectionView.reloadItems(at: [indexPath])
                    }
                }
            })
        }
    }
    
    //MARK: Scrollview delegate methods
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if(scrollView.contentOffset.y == (scrollView.contentSize.height - scrollView.frame.size.height))
        {
            //send next page request
            getProductsCatalogFor(page: (catalog?.page)! + 1)
        }
    }
}

