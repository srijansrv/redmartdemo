import Foundation
import UIKit

class ProductViewController: UIViewController {
    let imageBaseUrl = "http://media.redmart.com/newmedia/200p"
    var product: Product?
    var productProfile: ProductProfile?
    var error: Error?
    let manager = ServiceManager()
    
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UITextView!
    @IBOutlet weak var addToCartButton: UIButton!
    @IBOutlet weak var saveForLaterButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        activityIndicator.startAnimating()
        manager.getProductProfile(productId: (product?.id)!) { (productData, error) in
            self.productProfile = productData
            self.product = self.productProfile?.product
            self.error = error
            DispatchQueue.main.async { [unowned self] in
                self.updateUI()
            }
        }
    }
    
    func updateUI() {
        activityIndicator.stopAnimating()
        titleLabel.text = product?.title
        quantityLabel.text = product?.measure?.wtOrVol
        priceLabel.text = "\(product?.pricing?.price ?? 0)"
        descriptionLabel.text = product?.desc
        updateProductImage()
    }
    
    func updateProductImage() {
        var imageURL = imageBaseUrl
        let endPoint = product?.img?.name ?? ""
        imageURL = imageBaseUrl + endPoint
        
        manager.getImageForProduct(from: imageURL) { [unowned self] (image, error) in
            if( image != nil) {
                DispatchQueue.main.async {
                    self.productImage.image = image
                    self.productImage.contentMode = .scaleAspectFit
                }
            }
        }
    }
    
    @IBAction func closeButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
