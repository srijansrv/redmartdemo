import Foundation
import UIKit

class CatalogCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var savingsLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}


