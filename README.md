# RedMartDemo

Objective:
Build an iOS application which lists name, image and price of products. You are encouraged to implement it in a grid layout similar to the RedMart app available on AppStore (https://itunes.apple.com/us/app/redmart-grocery-shopping/id606780396?mt=8)
Show additional details about an individual product i.e. description etc. when tapped. Feel free to implement your design here.

API details:
API to download the list of products -> https://api.redmart.com/v1.6.0/catalog/search?theme=all-sales&pageSize=30&page=0
URL to concatenate the image URL in the API response to get the image -> http://media.redmart.com/newmedia/200p 
API to get additional info about a particular product → http://api.redmart.com/v1.6.0/products/{product_id}

Requirements:
·       The app should support iOS 9 and above
·       The code should be in Swift 3.2 or higher
·       Should use version control.
·        Frameworks or third-party libraries should be used only if there is a strong use-case and the use should be justified.

What we would like to see in your project?
·       iOS coding standards
·       Separation of concerns
·       Pagination
·       Unit Testing (extra points for automated testing!)
·       Custom views implementation
·       There are bonuses for transition between screens or collection View animations
