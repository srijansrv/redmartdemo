//
//  CatalogTest.swift
//  RedMartDemoTests
//
//  Created by Srijan Srivastava on 8/13/18.
//  Copyright © 2018 Srijan Srivastava. All rights reserved.
//

import XCTest

class CatalogTest: XCTestCase {
    
    func getCatalogFromMockFile() -> Catalog? {
        if let url = Bundle.main.url(forResource: "Catalog", withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let  decoder = JSONDecoder()
                decoder.keyDecodingStrategy = .convertFromSnakeCase
                let catalog = try decoder.decode(Catalog.self, from: data)
                return catalog
            } catch {
                print("error:\(error)")
                return nil
            }
        }
        return nil
    }
    
    func testCatalogJsonIsParsedCorrectly() {
        let catalog = getCatalogFromMockFile()
        XCTAssertEqual(catalog?.products?.count, 1)
        XCTAssertEqual(catalog?.facets?.categories?.first?.count, 221)
        XCTAssertEqual(catalog?.products?.first?.pricing?.discounts?.promo?.promoPrice, 5.57)
    }
    
}
