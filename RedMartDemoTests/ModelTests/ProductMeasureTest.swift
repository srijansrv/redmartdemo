//
//  ProductMeasureTest.swift
//  RedMartDemoTests
//
//  Created by Srijan Srivastava on 8/13/18.
//  Copyright © 2018 Srijan Srivastava. All rights reserved.
//

import XCTest

class ProductMeasureTest: XCTestCase {
    
    let measureJSON =
        """
    {
        "wt_or_vol":"42 g",
        "size":0.0
    }
    """.data(using: .utf8)
    
    func testProductMeasureJsonIsParsedCorrectly() {
        let  decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        let measure = try! decoder.decode(ProductMeasure.self, from: measureJSON!)
        
        XCTAssertEqual(measure.wtOrVol, "42 g")
        XCTAssertEqual(measure.size, 0.0)
    }
    
}
