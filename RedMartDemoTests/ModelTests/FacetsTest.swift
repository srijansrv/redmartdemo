//
//  FacetsTest.swift
//  RedMartDemoTests
//
//  Created by Srijan Srivastava on 8/13/18.
//  Copyright © 2018 Srijan Srivastava. All rights reserved.
//

import XCTest

class FacetsTest: XCTestCase {
    
    let facetsJSON =
        """
    {
    "categories": [
      {
        "id": 6928,
        "name": "RedMart Label",
        "uri": "redmart-label",
        "count": 221,
        "image": "/i/m/504445126400_1446443807223.jpg"
      },
      {
        "id": 243,
        "name": "Fruit & Veg",
        "uri": "fresh-produce",
        "count": 272,
        "image": "/i/m/8885003328014_0005_1520495600715.jpg"
      },
      {
        "id": 249,
        "name": "Frozen",
        "uri": "frozen",
        "count": 1593,
        "image": "/i/m/img_1530751444434.jpg"
      },
      {
        "id": 3039,
        "name": "Home & Outdoor",
        "uri": "home-outdoor",
        "count": 7991,
        "image": "/i/m/img_3014.jpg"
      }
    ]
    }
    """.data(using: .utf8)
    
    func testFacetsJsonIsParsedCorrectly() {
        let  decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        let facets = try! decoder.decode(Facets.self, from: facetsJSON!)
        
        XCTAssertEqual(facets.categories?.first?.count, 221)
        XCTAssertEqual(facets.categories?.last?.id, 3039)
        XCTAssertEqual(facets.categories![2].name, "Frozen")
    }
    
}
