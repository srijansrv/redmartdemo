//
//  ImageTest.swift
//  RedMartDemoTests
//
//  Created by Srijan Srivastava on 8/13/18.
//  Copyright © 2018 Srijan Srivastava. All rights reserved.
//

import XCTest

class ImageTest: XCTestCase {
    
    let imageJSON =
        """
    {
        "h":0,
        "w":0,
        "name":"/i/m/015000045630_0133_1525073492875.jpg",
        "position":0
    }
    """.data(using: .utf8)
    
    func testImageJsonIsParsedCorrectly() {
        let  decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        let imageData = try! decoder.decode(Image.self, from: imageJSON!)
        
        XCTAssertEqual(imageData.h, 0)
        XCTAssertEqual(imageData.w, 0)
        XCTAssertEqual(imageData.name, "/i/m/015000045630_0133_1525073492875.jpg")
        XCTAssertEqual(imageData.position, 0)
    }
    
    
}
