//
//  ProductFiltersTest.swift
//  RedMartDemoTests
//
//  Created by Srijan Srivastava on 8/13/18.
//  Copyright © 2018 Srijan Srivastava. All rights reserved.
//

import XCTest

class ProductFiltersTest: XCTestCase {
    let filtersJSON =
        """
    {
        "is_organic":0,
        "brand_uri":"gerber",
        "mfr_name":"Gerber",
        "frequency":168,
        "brand_name":"GERBER",
        "vendor_name":"SuperVal",
        "country_of_origin":"United States",
        "trending_frequency":85
    }
    """.data(using: .utf8)
    
    func testProductFiltersJsonIsParsedCorrectly() {
        let  decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        let productFilters = try! decoder.decode(ProductFilters.self, from: filtersJSON!)
        
        XCTAssertEqual(productFilters.isOrganic, 0)
        XCTAssertEqual(productFilters.brandUri, "gerber")
        XCTAssertEqual(productFilters.mfrName, "Gerber")
        XCTAssertEqual(productFilters.frequency, 168)
        XCTAssertEqual(productFilters.countryOfOrigin, "United States")
        XCTAssertEqual(productFilters.brandName, "GERBER")
        XCTAssertEqual(productFilters.trendingFrequency, 85)
    }
    
    
}
