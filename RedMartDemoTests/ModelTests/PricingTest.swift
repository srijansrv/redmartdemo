//
//  PricingTest.swift
//  RedMartDemoTests
//
//  Created by Srijan Srivastava on 8/13/18.
//  Copyright © 2018 Srijan Srivastava. All rights reserved.
//

import XCTest

class PricingTest: XCTestCase {
    
    let pricingJSON =
        """
    {
        "promo_id":222448,
        "on_sale":1,
        "price":7.95,
        "promo_price":5.57,
        "savings":30.0,
        "savings_amount":2.38,
        "savings_type":1,
        "savings_text":"30% OFF",
        "discounts":
        {
            "promo":
            {
                "promo_price":5.57,
                "savings":30.0,
                "savings_amount":2.38,
                "savings_type":1,
                "savings_text":"30% OFF"
            }
        },
        "applicable_discount":"promo"
    }
    """.data(using: .utf8)
    
    func testPricingJsonIsParsedCorrectly() {
        let  decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        let pricing = try! decoder.decode(Pricing.self, from: pricingJSON!)
        
        XCTAssertEqual(pricing.promoId, 222448)
        XCTAssertEqual(pricing.onSale, 1)
        XCTAssertEqual(pricing.discounts?.promo?.savingsType, 1)
    }
    
}
