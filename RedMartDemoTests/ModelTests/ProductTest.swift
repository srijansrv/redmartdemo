//
//  ProductTest.swift
//  RedMartDemoTests
//
//  Created by Srijan Srivastava on 8/13/18.
//  Copyright © 2018 Srijan Srivastava. All rights reserved.
//

import XCTest

class ProductTest: XCTestCase {
    
    let productJSON =
        """
    {
      "pricing": {
        "promo_id": 222448,
        "on_sale": 1,
        "price": 7.95,
        "promo_price": 5.57,
        "savings": 30,
        "savings_amount": 2.38,
        "savings_type": 1,
        "savings_text": "30% OFF",
        "discounts": {
          "promo": {
            "promo_price": 5.57,
            "savings": 30,
            "savings_amount": 2.38,
            "savings_type": 1,
            "savings_text": "30% OFF"
          }
        },
        "applicable_discount": "promo"
      },
      "id": 164600,
      "title": "GERBER Organic Cranberry Orange Puffs",
      "desc": "Snacks made with whole-grain goodness that melts in baby's mouth. Non-GMO Project Verified. No artificial flavors or colors. Puffed grain snack.",
      "measure": {
        "wt_or_vol": "42 g",
        "size": 0
      },
      "details": {
        "status": 1,
        "prod_type": 0,
        "uri": "gerber-organic-cranberry-orange-puffs-164600",
        "country_of_origin": "United States",
        "storage_class": "AmbientFB",
        "is_new": 0
      },
      "images": [
        {
          "h": 0,
          "w": 0,
          "name": "/i/m/015000045630_0133_1525073492875.jpg",
          "position": 0
        },
        {
          "h": 0,
          "w": 0,
          "name": "/i/m/015000045630_0134_1525073497017.jpg",
          "position": 1
        }
      ],
      "warehouse": {
        "measure": {
          "vol_metric_alt": "",
          "wt_metric": "g",
          "vol_metric": "",
          "wt": 42,
          "w": 0,
          "l": 0,
          "h": 0
        }
      },
      "sku": "015000045630",
      "filters": {
        "is_organic": 0,
        "brand_uri": "gerber",
        "mfr_name": "Gerber",
        "frequency": 168,
        "brand_name": "GERBER",
        "vendor_name": "SuperVal",
        "country_of_origin": "United States",
        "trending_frequency": 85
      },
      "categories": [
        4,
        34,
        1,
        17,
        9302,
        10490,
        14360,
        14377,
        14378,
        14905,
        14951
      ],
      "types": [
        129,
        2044
      ],
      "category_tags": [
        "baby",
        "baby-food",
        "snacks-34",
        "food-cupboard",
        "snacks",
        "childrens-snacks",
        "baby-flash-sale",
        "direct-retail",
        "mgm",
        "mgmf",
        "mgmm",
        "national-day-12",
        "toppicks"
      ],
      "img": {
        "h": 0,
        "w": 0,
        "name": "/i/m/015000045630_0133_1525073492875.jpg",
        "position": 0
      },
      "description_fields": {
        "primary": [
          {
            "name": "Country of Origin",
            "content": "United States"
          }
        ],
        "secondary": [
          {
            "name": "Ingredients",
            "content": "INGREDIENTS: ORGANIC WHOLE WHEAT FLOUR, ORGANIC RICE FLOUR, ORGANIC WHEAT STARCH, ORGANIC CANE SUGAR, DRIED ORGANIC APPLE PUREE, LESS THAN 1% OF: TRI- AND DICALCIUM PHOSPHATE, NATURAL CRANBERRY ORANGE FLAVOR (CONTAINS CITRIC AND ACETIC ACID), MIXED TOCOPHEROLS (TO MAINTAIN FRESHNESS), SUNFLOWER LECITHIN. VITAMINS AND MINERALS: VITAMIN E (ALPHA TOCOPHERYL ACETATE), IRON (ELECTROLYTIC). CONTAINS: WHEAT Always refer to the product packaging for the most accurate and up-to-date product information."
          }
        ]
      },
      "promotions": [
        {
          "id": 222448,
          "type": 1,
          "savings_text": "30% OFF",
          "promo_label": "",
          "current_product_group_id": 1
        }
      ],
      "pr": 1,
      "inventory": {
        "atp_status": 0,
        "max_sale_qty": 48,
        "qty_in_carts": 0,
        "qty_in_stock": 48,
        "stock_status": 1,
        "stock_type": 0,
        "atp_lots": [
          {
            "from_date": "2018-08-12T16:21:27Z",
            "to_date": "2018-10-13T15:59:59Z",
            "stock_status": 0,
            "qty_in_stock": 0,
            "qty_in_carts": 0
          },
          {
            "from_date": "2018-08-12T16:21:27Z",
            "to_date": "2018-12-07T15:59:59Z",
            "stock_status": 1,
            "qty_in_stock": 48,
            "qty_in_carts": 0
          }
        ],
        "next_available_date": "2018-08-12T16:21:27Z",
        "limited_stock_status": 0,
        "delivery_option": "STD"
      },
      "inventories": [
        {
          "atp_status": 0,
          "max_sale_qty": 48,
          "qty_in_carts": 0,
          "qty_in_stock": 48,
          "stock_status": 1,
          "stock_type": 0,
          "atp_lots": [
            {
              "from_date": "2018-08-12T16:21:27Z",
              "to_date": "2018-10-13T15:59:59Z",
              "stock_status": 0,
              "qty_in_stock": 0,
              "qty_in_carts": 0
            },
            {
              "from_date": "2018-08-12T16:21:27Z",
              "to_date": "2018-12-07T15:59:59Z",
              "stock_status": 1,
              "qty_in_stock": 48,
              "qty_in_carts": 0
            }
          ],
          "next_available_date": "2018-08-12T16:21:27Z",
          "limited_stock_status": 0,
          "delivery_option": "STD"
        }
      ]
    }
    """.data(using: .utf8)
    
    func testProductJsonIsParsedCorrectly() {
        let  decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        let product = try! decoder.decode(Product.self, from: productJSON!)
        
        XCTAssertEqual(product.id, 164600)
        XCTAssertEqual(product.desc, "Snacks made with whole-grain goodness that melts in baby's mouth. Non-GMO Project Verified. No artificial flavors or colors. Puffed grain snack.")
        XCTAssertEqual(product.title, "GERBER Organic Cranberry Orange Puffs")
        XCTAssertEqual(product.pricing?.promoId, 222448)
        XCTAssertEqual(product.pricing?.discounts?.promo?.savingsAmount, 2.38)
        XCTAssertEqual(product.measure?.wtOrVol, "42 g")
        XCTAssertEqual(product.details?.storageClass, "AmbientFB")
        XCTAssertEqual(product.details?.countryOfOrigin, "United States")
        XCTAssertEqual(product.images?.first?.position, 0)
        XCTAssertEqual(product.warehouse?.measure.wt, 42)
        XCTAssertEqual(product.sku, "015000045630")
        XCTAssertEqual(product.filters?.isOrganic, 0)
        XCTAssertEqual(product.filters?.mfrName, "Gerber")
        XCTAssertEqual(product.filters?.tasteProfile, nil)
        XCTAssertEqual(product.categories![4], 9302)
        XCTAssertEqual(product.types?.last, 2044)
        XCTAssertEqual(product.categoryTags![8], "mgm")
        XCTAssertEqual(product.img?.name, "/i/m/015000045630_0133_1525073492875.jpg")
        XCTAssertEqual(product.descriptionFields?.primary.first?.content, "United States")
        XCTAssertEqual(product.descriptionFields?.secondary.last?.name, "Ingredients")
        XCTAssertEqual(product.promotions?.first?.currentProductGroupId, 1)
        XCTAssertEqual(product.promotions?.last?.savingsText, "30% OFF")
        XCTAssertEqual(product.pr, 1)
        XCTAssertEqual(product.inventory?.maxSaleQty, 48)
        XCTAssertEqual(product.inventory?.atpLots?.last?.qtyInStock, 48)
        XCTAssertEqual(product.inventory?.nextAvailableDate, "2018-08-12T16:21:27Z")
        XCTAssertEqual(product.inventory?.deliveryOption, "STD")
        XCTAssertEqual(product.inventories?.first?.stockStatus, 1)
        XCTAssertEqual(product.inventories?.first?.atpLots?.last?.qtyInCarts, 0)
    }
    
}
