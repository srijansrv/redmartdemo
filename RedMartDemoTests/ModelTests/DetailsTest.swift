//
//  DetailsTest.swift
//  RedMartDemoTests
//
//  Created by Srijan Srivastava on 8/13/18.
//  Copyright © 2018 Srijan Srivastava. All rights reserved.
//

import XCTest

class DetailsTest: XCTestCase {
    
    let detailsJSON =
    """
    {
        "status": 1,
        "prod_type": 0,
        "uri": "gerber-organic-cranberry-orange-puffs-164600",
        "country_of_origin": "United States",
        "storage_class": "AmbientFB",
        "is_new": 0
    }
    """.data(using: .utf8)
    
    func testDetailsJsonIsParsedCorrectly() {
        let  decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        let productDetails = try! decoder.decode(Details.self, from: detailsJSON!)
        
        XCTAssertEqual(productDetails.status, 1)
        XCTAssertEqual(productDetails.prodType, 0)
        XCTAssertEqual(productDetails.isNew, 0)
        XCTAssertEqual(productDetails.uri, "gerber-organic-cranberry-orange-puffs-164600")
        XCTAssertEqual(productDetails.countryOfOrigin, "United States")
        XCTAssertEqual(productDetails.storageClass, "AmbientFB")
    }
    
}
