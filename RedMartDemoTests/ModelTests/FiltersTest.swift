//
//  FiltersTest.swift
//  RedMartDemoTests
//
//  Created by Srijan Srivastava on 8/13/18.
//  Copyright © 2018 Srijan Srivastava. All rights reserved.
//

import XCTest

class FiltersTest: XCTestCase {
    
    let filtersJSON =
        """
    {
    "options": [
      {
        "name": "Brands",
        "uri": "brand",
        "types": [
          {
            "count": 611,
            "name": "ICON",
            "uri": "icon"
          },
          {
            "count": 438,
            "name": "Biofinest",
            "uri": "biofinest"
          },
          {
            "count": 373,
            "name": "Mercury Goospery",
            "uri": "mercury-goospery"
          },
          {
            "count": 342,
            "name": "Fullmark",
            "uri": "fullmark"
          },
          {
            "count": 308,
            "name": "Revlon",
            "uri": "revlon"
          },
          {
            "count": 293,
            "name": "STABILO",
            "uri": "stabilo"
          },
          {
            "count": 288,
            "name": "The Paper Stone",
            "uri": "the-paper-stone"
          },
          {
            "count": 272,
            "name": "L.A. Girl",
            "uri": "la-girl"
          },
          {
            "count": 270,
            "name": "Creative Converting",
            "uri": "creative-converting"
          },
          {
            "count": 255,
            "name": "Max & Molly",
            "uri": "max--molly"
          },
          {
            "count": 200,
            "name": "RedMart",
            "uri": "redmart"
          },
          {
            "count": 198,
            "name": "Colgate",
            "uri": "colgate"
          },
          {
            "count": 61,
            "name": "Palais Suite",
            "uri": "palais-suite"
          }
        ]
      },
      {
        "name": "Price",
        "uri": "price",
        "types": [
          {
            "count": 2613,
            "name": "<$3",
            "uri": "0-2.99"
          },
          {
            "count": 3369,
            "name": "$3-$5",
            "uri": "3-5"
          },
          {
            "count": 6835,
            "name": "$5-$10",
            "uri": "5-10"
          },
          {
            "count": 10684,
            "name": "$10-$25",
            "uri": "10-25"
          },
          {
            "count": 12921,
            "name": ">$25",
            "uri": "25.01-"
          }
        ]
      }
    ],
    "toggles": [
      {
        "name": "All",
        "uri": "all"
      },
      {
        "name": "New",
        "uri": "is_new"
      },
      {
        "name": "On Sale",
        "uri": "on_sale"
      },
      {
        "name": "In Stock",
        "uri": "stock_status"
      }
    ]
    }
    """.data(using: .utf8)
    
    func testFiltersJsonIsParsedCorrectly() {
        let  decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        let filters = try! decoder.decode(Filters.self, from: filtersJSON!)
        
        XCTAssertEqual(filters.options?.first?.name, "Brands")
        XCTAssertEqual(filters.options?.first?.types![11].name, "Colgate")
        XCTAssertEqual(filters.toggles![2].name, "On Sale")
    }
    
}
